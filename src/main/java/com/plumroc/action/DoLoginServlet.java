package com.plumroc.action;

import com.plumroc.constatns.Constants;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 调用微博的登录API地址
 * Call the login API address of Weibo
 *
 * @author PlumRoc
 */
@WebServlet("/dologin")
public class DoLoginServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //redirect weibo login url
        resp.sendRedirect(Constants.URL);
    }
}
