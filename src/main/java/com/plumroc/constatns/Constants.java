package com.plumroc.constatns;

/**
 * 微博常量类
 *
 * @author PlumRoc
 */
public class Constants {

    /**
     * App Key
     */
    public static final String CLIENT_ID = "208878435";

    /**
     * App Secret
     */
    public static final String CLIENT_SECRET = "f2d4fdd76dbcb1f8d6c63bb498293c53";

    /**
     * 授权回调页
     */
    public static final String REDIRECT_URI = "http://127.0.0.1:8080/authWeibo";

    /**
     * 用户授权的唯一票据
     */
    public static final String GET_TOKEN_URL = "https://api.weibo.com/oauth2/access_token";

    /**
     * 用户备注信息
     */
    public static final String GET_USER_INFO = "https://api.weibo.com/2/users/show.json";

    /**
     * 微博认证网址
     */
    public static final String URL = "https://api.weibo.com/oauth2/authorize?client_id=" + CLIENT_ID + "&response_type=code&redirect_uri=" + REDIRECT_URI;

}
