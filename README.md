# 📚 项目介绍

​		一款 Java Web + Maven 框架精心打造的微博第三方登陆,项目使用Servlet版本,可以更改成自己想要的框架。



## 🍪 内置模块

```
├── pom.xml
└── src
     └── main  
        ├── java
        │   └── plumroc
        │       ├── action
        │       │   ├── DoLoginServlet.java
        │       │   └── QueryWeiBo.java
        │       └── constatns
        │           └── Constants.java
        └── webapp
            ├── index.jsp
            ├── auth.jsp
            │      
            └── WEB-INF 
     		     └── web.xml
```



## 👷 开发者信息

- 作者：李鹏
- 出处：个人



## 🎨 修改常量

需要用户自行修改的: **Constants**部分常量

`注：一般更改 App Key ,App Secret ,授权回调页即可`